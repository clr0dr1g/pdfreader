package celio.rodrigues.pdfreader;

import java.io.File;
import java.io.FileNotFoundException;

import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends Activity{

	EditText editText;
	String caminho;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		editText = (EditText) findViewById(R.id.editText1);
	}
	
	public void AbrirArquivo(View v){
		caminho = editText.getText().toString();
		File arquivo = new File(caminho);
		if(arquivo != null){
			if(arquivo.isFile()){
				if(arquivo.getName().contains(".pdf")){
					Intent it = new Intent(getBaseContext(), Leitor.class);
					it.putExtra("pdf", caminho);
					startActivity(it);
				}else{
					Toast.makeText(getBaseContext(), "Impossivel Abrir", Toast.LENGTH_SHORT).show();
				}				
			}else{
				Toast.makeText(getBaseContext(), "Impossivel Abrir", Toast.LENGTH_SHORT).show();
			}
		}else{
			Toast.makeText(getBaseContext(), "Impossivel Abrir", Toast.LENGTH_SHORT).show();
		}	
	}
}

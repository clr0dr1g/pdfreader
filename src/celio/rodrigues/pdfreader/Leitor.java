package celio.rodrigues.pdfreader;

import java.io.File;
import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;

public class Leitor extends Activity implements OnPageChangeListener{
	
	ActionBar bar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_leitor);
		
		bar = getActionBar();
		
		String caminho = this.getIntent().getStringExtra("pdf");
		
		File arquivo = new File(caminho);
		
		if(arquivo != null){
			if(arquivo.isFile()){

				PDFView pdf = (PDFView) findViewById(R.id.pdfView);

				pdf.fromFile(arquivo)
					.defaultPage(1)
					.onPageChange(this)
					.enableSwipe(true)
					.swipeVertical(true)
					.load();			
			}
		}
	}

	@Override
	public void onPageChanged(int page, int TotalPages) {
		bar.setTitle("Página " + page + " de " + TotalPages);		
	}
}

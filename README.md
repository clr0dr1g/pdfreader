#**PDFReader** é um aplicativo leitor de pdf desenvolvido com a lib  ```Android PDFView``` de Joan Zapata

#Este aplicativo é dependente de Android PDFView.
#
#Faça o download da lib em [Android PDFView](https://github.com/JoanZapata/android-pdfview).
#
#Se você estiver utilizando o Eclipse IDE use este [Android PDFView for Eclipse](https://bitbucket.org/clr0dr1g/android-pdfview-for-eclipse)

# License

```
Copyright 2013-2015 Joan Zapata

This file is part of Android-pdfview.

Android-pdfview is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Android-pdfview is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Android-pdfview.  If not, see <http://www.gnu.org/licenses/>.
```